#ifndef BSPLINE_H_
#define BSPLINE_H_

#include <nifti1_io.h>
#include <Eigen/Dense>
#include <unsupported/Eigen/Polynomials>
#include <enoki/array.h>
#include <enoki/dynamic.h>
#include <enoki/autodiff.h>

#include <array>
#include <algorithm>
#include <chrono>
#include <cmath>
#include <iostream>
#include <map>
#include <memory>
#include <numeric>
#include <utility>
#include <vector>

namespace {
    // prints out an iterator type
    template <class T>
    void print(const T& array) {
        for (auto& i : array) 
            std::cout << i << " ";
        std::cout << std::endl;
    }

    // Create a factorial function (to generate factorials)
    template <class T>
    constexpr T factorial(T n) {
        if (n == 0) // base case
            return 1;
        // return the factorial of n
        return n*factorial(n-1);
    }

    // Create a factorial function that stops at value k
    template <class T>
    constexpr T factorial_stop (T n, T k) {
        if (n == k) // base case 1
            return 1;
        else if (n-1 == k) // base case 2
            return n;
        // get the factorial_stop of (n,k)
        return n*factorial_stop(n-1, k); 
    }

    // n choose k
    template <class T>
    constexpr T nCk(T n, T k) {
        return factorial_stop(n, k)/factorial(n-k);
    }

    // constexpr reduce
    template <class T, class Iterator>
    constexpr T reduce(Iterator begin, Iterator end) {
        T sum = 0;
        for (auto it = begin; it != end; it++)
            sum += *it;
        return sum;
    }
}

template <class FloatingPoint>
class BSpline {
    public:
        virtual int getOrder() = 0;
        virtual FloatingPoint getRadius() = 0;
        virtual FloatingPoint BSpline6(FloatingPoint) = 0;
        virtual FloatingPoint evaluate(FloatingPoint) = 0;
        virtual unsigned long long int getNormalizationConstant() = 0;
        virtual std::vector<FloatingPoint> getPoles() = 0;
        virtual ~BSpline() {};
};

template <class FloatingPoint, int order>
class BSplineN : public BSpline<FloatingPoint> {
    public:
        BSplineN() {
            for (auto& v : bspline_coefficients)
                print(v);

            constexpr int data_size = 100000000;
            constexpr double increment = 4.0 / static_cast<double>(data_size);
            using DPack = enoki::Packet<double,1>;
            using DynArray = enoki::DynamicArray<DPack>;
            std::cout << "Constructing Test Array..." << std::endl;
            DynArray x = enoki::arange<DynArray>(data_size) * increment;
            std::cout << "Done!" << std::endl;

            double test1 = 0.0; // dummy value
            auto start1 = std::chrono::steady_clock::now();
            for (auto& v : x) {
                test1 += BSpline6(v);
            }
            auto finish1 = std::chrono::steady_clock::now();

            double test2 = 0.0; // dummy value
            auto start2 = std::chrono::steady_clock::now();
            for (auto& v : x) {
                test2 += evaluate(v);
            }
            auto finish2 = std::chrono::steady_clock::now();

            double elapsed1 = std::chrono::duration_cast<std::chrono::duration<double> >(finish1 - start1).count();
            double elapsed2 = std::chrono::duration_cast<std::chrono::duration<double> >(finish2 - start2).count();
            std::cout << "BSpline6 dummy: " << test1 << std::endl;
            std::cout << "BSpline6 Time: " << elapsed1 << std::endl;
            std::cout << "evaluate dummy with order " << order << ": " << test2 << std::endl;
            std::cout << "evaluate Time with order " << order << ": " << elapsed2 << std::endl;
        };
        
        // spline order
        static constexpr int bspline_order{ order };
        int getOrder() override { return bspline_order; }

        // spline radius
        static constexpr FloatingPoint radius = 0.5*(order+1);
        FloatingPoint getRadius() override { return radius; }

        FloatingPoint BSpline6(FloatingPoint x) override {
            x = fabs(x);
            if(x <= 0.5) {
                return (367.9375 + (0 + (-288.75 + (0 + (105 + (0 - 20*x)*x)*x)*x)*x)*x);
            }
            if(x < 1.5) {
                x = 1.5 - x;
                return (57 + (150 + (135 + (20 + (-45 + (-30 + 15*x)*x)*x)*x)*x)*x);
            }
            if(x < 2.5) {
                x = 2.5 - x;
                return (1 + (6 + (15 + ( 20 + (15 + (6 - 6*x)*x)*x)*x)*x)*x);
            }
            if(x < 3.5) {
                x = 3.5 - x;
                return x*x*x*x*x*x;
            }
            return 0;
        }

        // evaluate function
        FloatingPoint evaluate(FloatingPoint x) override {
            // Since bspline is symmetric, grab absolute value of x
            x = fabs(x);

            // get initial knot based on spline order
            FloatingPoint t = t1; // initialized to 0.5 for even order and 1.0 for odd order

            // Loop over each interval of spline polynomial coefficients
            // calling Horner's method if x falls in the domain of the knot interval
            // bspline_coefficients is a std::vector<std::vector<double>> type holding
            // the piecewise polynomial coefficients for each knot interval
            // For example, bspline_coefficients for bspline order 6 looks like:
            // {
            //    {367.9375, 0, -288.75, 0, 105, 0, -20},
            //    {57, 150, 135, 20, -45, -30, 15},
            //    {1, 6, 15, 20, 15, 6, -6},
            //    {0, 0, 0, 0, 0, 0, 1}
            // }
            for (auto& poly_coeffs : bspline_coefficients) { // grab reference to polynomial coefficients for interval
                if (x < t) { // x is in domain
                    if (x >= t1) // checks if whether this is after the 1st knot
                        x = t - x; // if not in the first knot, use t - x
                    // compute polynomial using Horner's method to get the value at x
                    // we use reverse iterator since coefficients are stored in ascending order x^0 ... x^n
                    // initialize accumulate to last value which is coefficient for x^n
                    return std::accumulate(poly_coeffs.rbegin()+1, poly_coeffs.rend(), poly_coeffs.back(),
                        [&x] (FloatingPoint& c, const FloatingPoint& n) { return std::move(c)*x + n; });
                }
                // increment to next knot
                t += 1.0;
            }

            // x is not in domain, so return 0
            return 0;
        };
    private:
        // Algorithm 9
        // This finds the complex polynomial coefficients so that we can solve for the poles of the prefilter
        static constexpr std::array<FloatingPoint, 2*(order/2)+1> getComplexPolynomialCoefficients() {
            constexpr int n = order;
            if (n == 0)
                return {1};
            constexpr int n_tilde = n/2;

            // initialize vectors for b and d
            // first index gives order n, while second gives polynomial coefficient
            // I'm too lazy to figure out the correct size here so let's just
            // initialize to 100 x 100 for now...
            std::array<std::array<FloatingPoint, 100>, 100> b{0};
            std::array<std::array<FloatingPoint, 100>, 100> d{0};

            // initialize with order n = 0
            b[0][0] = 1;
            d[0][0] = 0.5;
            
            // solce b_k^{m} and d_k^(m) recursively
            for (int m = 1; m < n; m++) {
                // define m_tilde
                int m_tilde = m/2;

                for (int k = 0; k <= m_tilde; k++) {
                    b[m][k] = (1.0/m) * (((m+1)*0.5 + k)*d[m-1][k] + ((m+1)*0.5 - k)*d[m-1][k-1 == -1 ? 0 : k-1]);
                    d[m][k] = (1.0/m) * (((m+2)*0.5 + k)*b[m-1][k+1] + (m*0.5 - k)*b[m-1][k]);
                }
            }

            // do b_k^(n)
            for (int k = 0; k <= n_tilde; k++)
                b[n][k] = (1.0/n) * (((n+1)*0.5 + k)*d[n-1][k] + ((n+1)*0.5 - k)*d[n-1][k-1 == -1 ? 0 : k-1]);

            // return polynomial coefficients (duplicate some coefficients since they have z^n^tilde+i and z^n^tilde-i)
            std::array<FloatingPoint, 2*(n/2)+1> coefficients{0};
            auto cit = coefficients.begin();
            for (auto it = b[n].begin() + n_tilde; it != b[n].begin()-1; it--) {
                *cit = *it; cit++;
            }
            cit = coefficients.begin() + n_tilde+1;
            for (auto it = coefficients.begin() + n_tilde - 1; it != coefficients.begin() - 1; it--) {
                *cit = *it;
                cit++;
            }
            return coefficients; 
        };

        // Uses a Polynomial solver to grab the poles of the complex polynomial expression
        static std::array<FloatingPoint, std::max(order/2, 1)> computePoles(
            std::array<FloatingPoint, 2*(order/2)+1> complex_polynomial_coefficients) {
                // get order as n
                constexpr int n = order;

                // convert to Eigen Matrix type so we can use Eigen's polynomial solver
                auto poly_coeff = Eigen::Map<Eigen::Matrix<FloatingPoint, 2*(order/2)+1, 1>>(
                    complex_polynomial_coefficients.data(), complex_polynomial_coefficients.size());

                // create array to store poles
                std::array<FloatingPoint, std::max(order/2, 1)> poles{{0}};
                auto poles_it = poles.begin();

                // find roots of the polynomial
                if (n > 1) { // roots only exists for order > 1
                    Eigen::PolynomialSolver<FloatingPoint, std::max(2*(order/2), 1)> solver(poly_coeff);
                    auto roots = solver.roots();
                    for (int i = 0; i < roots.rows(); i++)
                        if (roots[i].real() < 0 && roots[i].real() > -1.0) { // get valid poles
                            *poles_it = roots[i].real();
                            poles_it++;
                        }
                    // Eigen reports roots from smallest to largest, reverse so it becomes largest to smallest
                    std::reverse(poles.begin(), poles.end()); // TODO: order may not matter...
                }

                // return poles
                return poles;
            };
        
        // Compute the normalization constant
        static constexpr unsigned long long int computeNormalizationConstant() {
            if (order % 2 == 0) // even
                return std::pow<unsigned long long int>(2, order);
            return 1; // odd
        };

        // Generate BSpline coefficients
        static constexpr std::array<std::array<FloatingPoint, order+1>, order/2+1> getBSplineCoefficients() {
            constexpr int n = order;
            constexpr int n_tilde = n/2;

            // Create C^(n)_k,j store
            std::array<std::array<FloatingPoint, n+1>, n_tilde> Cn_kj{0};

            // Create D^(n)_ntilde,j store
            std::array<FloatingPoint, n+1> Dn_ntj{0};

            // Compute C^(n)_k,j
            std::array<long long int, n_tilde+1> temp{0};
            for (int k = 0; k < n_tilde; k++) {
                for (int j = 0; j <= n; j++) {
                    if (j == n) {
                        for (int i = 0; i <= k; i++)
                            temp[i] = std::pow(-1, i)*nCk(n+1, i);
                        Cn_kj[k][j] = reduce<FloatingPoint>(temp.begin(), temp.begin()+k+1);
                    }
                    else {
                        for (int i = 0; i <= k-1; i++)
                            temp[i] = std::pow(-1, i)*nCk(n+1, i)*std::pow(k-i, n-j);
                        Cn_kj[k][j] = nCk(n, j)*reduce<FloatingPoint>(temp.begin(), temp.begin()+k);
                    }
                }
            }

            // Compute D^(n)_ntilde,j
            for (int j = 0; j <= n; j++) {
                for (int i = 0; i <= n_tilde; i++)
                    temp[i] = std::pow(-1, i+j)*nCk(n+1, i)*std::pow(n + 1 - 2*i, n-j);
                Dn_ntj[j] = (1/std::pow(2, n-j))*nCk(n, j)*reduce<FloatingPoint>(temp.begin(), temp.begin()+n_tilde+1);
            }

            // combine coefficients
            std::array<std::array<FloatingPoint, n+1>, n_tilde+1> coefficients{0};
            coefficients[0] = Dn_ntj;
            auto cit = coefficients.begin() + 1;
            for (auto it = Cn_kj.rbegin(); it != Cn_kj.rend(); it++) {
                *cit = *it; cit++;
            }
            return coefficients;
        };

        // first knot of interpolator
        static constexpr FloatingPoint t1{ order % 2 == 0 ? 0.5 : 1.0 };
    public:
        // normalization constant
        static constexpr unsigned long long int normalization_constant{computeNormalizationConstant()};
        unsigned long long int getNormalizationConstant() override { return normalization_constant; };
        
        // complex coefficients
        static constexpr std::array<FloatingPoint, 2*(order/2)+1> complex_polynomial_coefficients{
            getComplexPolynomialCoefficients() };

        // poles
        std::array<FloatingPoint, std::max(order/2, 1)> poles{
            computePoles(complex_polynomial_coefficients) };
        std::vector<FloatingPoint> getPoles() override { 
            return std::vector<FloatingPoint>(poles.begin(), poles.end()); };
        
        // bspline polynomial coefficients
        static constexpr std::array<std::array<FloatingPoint, order+1>, order/2+1> bspline_coefficients{
            getBSplineCoefficients() };
};

class Interpolator {
    public:
        using nii_img = nifti_image;
        using nii_img_ptr = std::shared_ptr<nifti_image>;
        Interpolator(nii_img_ptr, int = 3, double = 1e-6);
        std::shared_ptr<BSpline<double>> bspline;
        std::vector<int> dimensions;
        std::vector<int> extended_dims;
        std::vector<float> data;
        std::shared_ptr<int> ext(int, int, int);
    private:
        int shift = 0;
        std::shared_ptr<double> xBuf;
        std::shared_ptr<double> yBuf;
        std::shared_ptr<double> zBuf;
        std::vector<int> truncation;
        std::vector<int> Lprecision;
        std::vector<int> compute_truncation(double);
        std::vector<double> compute_mu(std::vector<double>, int);
        std::vector<int> compute_Lprecision();
        std::vector<double> prefiltering();
        std::vector<double> apply_exponential_filter(std::vector<double>, std::vector<double>);
    public:
        std::vector<double> prefiltered_image;
};

#endif
