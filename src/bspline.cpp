#include <bspline.h>

// Define constructor for Interpolator
Interpolator::Interpolator(nii_img_ptr img, int order, double eps) :
    bspline(std::make_shared<BSplineN<double, 6>>())//, // initialize bspline object
    // dimensions(std::begin(img->dim), std::end(img->dim)), // initialize dimensions of image
    // extended_dims(dimensions), // initialize dimensions for plan; copy from image dims
    // data(static_cast<float*>(img->data), static_cast<float*>(img->data) + img->nx*img->ny*img->nz), // get image data
    // truncation(compute_truncation(eps)), // initialize truncation
    // Lprecision(compute_Lprecision()) // initialize Lprecision
    {
        // std::cout << bspline->getOrder() << std::endl;
        // std::cout << bspline->getRadius() << std::endl;
        // print(bspline->getPoles());
    }

// Compute truncation
std::vector<int> Interpolator::compute_truncation(double eps) {
    int n_tilde = bspline->getOrder()/2;
    if (n_tilde < 1) // just return 0 if order 0/1
        return {0};

    std::vector<double> poles = bspline->getPoles();
    int img_dim = dimensions[0];
    std::vector<int> truncation(n_tilde);
    std::vector<double> mu(compute_mu(poles, n_tilde));
    
    // Compute rho^(n)
    double rhon = 1.0;
    for (int i = 0; i < n_tilde; i++)
        rhon *= (1 + poles[i]) / (1 - poles[i]);
    rhon *= rhon;

    // Get truncation indices
    double log_eps_cte = log(eps * rhon * rhon / img_dim); // Divide by the image dimension for correct bound
    double alpha;
    double prod_mu = 1;
    for (int i = n_tilde-1; i >= 0; i--) {
        alpha = poles[i];
        truncation[i] = 1 + floor((log_eps_cte + log((1-alpha)*(1-mu[i])*prod_mu))/log(-alpha));
        prod_mu *= mu[i];
    }

    return truncation;
}

// Compute mu
std::vector<double> Interpolator::compute_mu(std::vector<double> poles, int n_tilde) {
    std::vector<double> mu(n_tilde);
    double sum = 0;
    double log_pole = log(-poles[0]);

    // loop through mu array
    mu[0] = 0;
    for (int i = 1; i < n_tilde; i++) {
        sum += 1.0/log_pole;
        log_pole = log(-poles[i]);
        mu[i] = log_pole * sum / (1 + log_pole *  sum);
    }

    // return mu
    return mu;
}

std::vector<int> Interpolator::compute_Lprecision() {
    int n_tilde = bspline->getOrder()/2;
    std::vector<int> Lprecision(n_tilde+1);
    Lprecision[n_tilde] = n_tilde;
    for(int i = n_tilde-1; i >= 0; i--) {
        Lprecision[i] = Lprecision[i+1] + truncation[i];
    }
    shift = Lprecision[0];
    for (int i = 0; i < extended_dims[0]; i++) // loop over dims
        extended_dims[i+1] += 2*shift;
    return Lprecision;
}
