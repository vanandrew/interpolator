#include <bspline.h>

#include <nifti1_io.h>
// #include <matplotlibcpp.h>

#include <algorithm>
#include <array>
#include <iostream>
#include <memory>
#include <string>
#include <stdlib.h>
#include <vector>

int main(int argc, char **argv) {
    // validate inputs
    if (argc != 3)
        return 1;
    
    // read in a nifti file
    std::string nifti_file = argv[1];
    std::cout << nifti_file << std::endl;
    std::shared_ptr<nifti_image> img(nifti_image_read(nifti_file.c_str(), 1));
    if( !img ) {
      return 2;
    }

    // Declare interpolation plan
    int order = atoi(argv[2]);
    Interpolator interp(img, order);

    return 0;
}
